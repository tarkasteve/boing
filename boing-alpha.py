#!/usr/bin/python

import sys, time
from math import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

def keyboard(*args):
    if args[0] == '\033':
        sys.exit()

def init():
    global bounce, rot, x, xinc, bounceinc, rotinc, xmin, xmax
    bounce = 90
    rot = 0
    xmin = 3.5
    xmax = 16 - 3.5
    x = xmin + xmax * 0.25
    xinc = 2.6
    bounceinc = 90.0
    rotinc = 40.0

    glLineWidth(2)
    
    gengrid()
    genball(9, 16)

    # Framerate control
    global now, frames
    now = time.time()
    frames = 0

def gengrid():
    global gridlist
    gridlist = glGenLists(1)
    glNewList (gridlist, GL_COMPILE)

    glBegin(GL_LINES)
    glColor4d(0.63, 0, 0.63, 1)
    for i in range(0,16+1):
        glVertex3f(0, i, 0)
        glVertex3f(16, i, 0)
        glVertex3f(i, 0, 0)
        glVertex3f(i, 16, 0)
    glEnd()
    glEndList ()

def genball(lats, longs):
    sect = 180 / lats
    rsect = radians(sect)
    lsect = 360.0 / longs

    polys = [[],[]]
    quads = [[],[]]
    cindex = 0

    # glEnable(GL_CULL_FACE)
    # glFrontFace(GL_CCW)
    # glCullFace(GL_BACK)

    # Top
    topv = [0, 0, 1]
    scale = sin(radians(sect))
    angle = 0.0
    while angle <= 360.0 - lsect:
        cindex ^= 1
        polys[cindex] += [[topv,
                          [cos(radians(angle)) * scale,
                           sin(radians(angle)) * scale,
                           cos(rsect)],
                          [cos(radians(angle+lsect)) * scale,
                           sin(radians(angle+lsect)) * scale,
                           cos(rsect)]]]
        angle += lsect

    # Body of quad strips
    for step in range(1,lats-1):
        cindex ^= 1
        zu = cos(radians(sect * step))
        zl = cos(radians(sect * (step + 1)))
        scaleu = sin(rsect * step)
        scalel = sin(rsect * (step+ 1))
        angle = 0.0
        while angle < 360.0:            
            cup = cos(radians(angle))
            sup = sin(radians(angle))
            cup2 = cos(radians(angle + lsect))
            sup2 = sin(radians(angle + lsect))
            cindex ^= 1
            quads[cindex] += [[[cup * scaleu, sup * scaleu, zu],
                               [cup * scalel, sup * scalel, zl],
                               [cup2 * scalel, sup2 * scalel, zl],
                               [cup2 * scaleu, sup2 * scaleu, zu]]]
            angle += lsect

    # Bottom
    bv = [0, 0, -1]
    scale = sin(rsect * (lats-1))
    angle = 360.0
    while angle >= 0.0:
        cindex ^= 1
        polys[cindex] += [[bv,
                          [cos(radians(angle)) * scale,
                           sin(radians(angle)) * scale,
                           cos(rsect * (lats - 1))],
                          [cos(radians(angle-lsect)) * scale,
                           sin(radians(angle-lsect)) * scale,
                           cos(rsect * (lats - 1))]]]
        angle -= lsect

    global balllist
    balllist = glGenLists(1)
    glNewList(balllist, GL_COMPILE)


    def dosect(idx):
        for l in polys[idx]:
            glBegin(GL_POLYGON)
            glVertex3fv(l[0])
            glVertex3fv(l[1])
            glVertex3fv(l[2])
            glEnd()
        for l in quads[idx]:
            glBegin(GL_QUADS)
            glVertex3fv(l[0])
            glVertex3fv(l[1])
            glVertex3fv(l[2])
            glVertex3fv(l[3])
            glEnd()

    glEnable(GL_BLEND)
    glEnable(GL_DEPTH_TEST)
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    glDepthMask(GL_TRUE)
    glColor4f(1,1,1,1)
    dosect(0)

    glDepthMask(GL_FALSE)
    glColor4f(0.94, 0, 0, 0.5)
    dosect(1)

    # Reset to safe state
    glDepthMask(GL_TRUE)
    glDisable(GL_CULL_FACE)    

    glEndList()

def reshape(w, h):
    glViewport(0, 0, w, h)
    glMatrixMode (GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, w/h, 1, 25.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def drawgrid():
    glPushMatrix()
    
    glCallList(gridlist)
    glTranslatef(0, 16, 0)
    glRotatef(90,1,0,0)
    glCallList(gridlist)

    glPopMatrix()

def drawball():
    global bounce, rot, x, bounceinc, rotinc, xinc, now

    last = now
    now = time.time()
    inc =  now - last
    bounce += bounceinc * inc 
    if bounce > 180: bounce = 0
    rot += rotinc * inc
    if rot >= 360: rot = 0
    x += xinc * inc
    if x <= xmin or x >= xmax: xinc = -xinc
    
    glPushMatrix()

    glTranslatef(x, 8 + 4.5, 7 * sin(radians(bounce)) + 3.5)
    glRotatef(22, 0, 1, 0)
    glRotatef(rot, 0, 0, 1)
    glColor4f(1,1,1,1)
    glScalef(3.5, 3.5, 3.5)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    glCallList(balllist)

    glPopMatrix()

def display():
    global frames
    frames += 1

    glDrawBuffer(GL_BACK)
    glClearColor(0.63, 0.63, 0.63, 1)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glLoadIdentity()
    gluLookAt (8, -6, 8,
               8, 4, 8,
               0, 0, 1)

    drawgrid()
    drawball()

    glutSwapBuffers()

def nextframe():
    glutPostRedisplay()

def timer(val):
    global frames
    print frames, "FPS"
    frames = 0
    glutTimerFunc(1000, timer, 0)

def main():
    glutInit(sys.argv)

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH)
    glutInitWindowSize(480, 480)
    glutCreateWindow("Test Window")

    init()

    glutKeyboardFunc(keyboard)
    glutReshapeFunc(reshape)
    glutDisplayFunc(display)
    glutIdleFunc(nextframe)
    glutTimerFunc(1000, timer, 0)

    glutMainLoop()

main()

